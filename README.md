# Framabin SH

Framabin SelfHosted Version. Version de framabin allégée de framanav. Vous pouvez ainsi avoir une version auto-hébergée française utilisant bootstrap avec les améliorations graphiques proposées par framasoft.

* * *

Framabin est la version traduite et personnalisée du logiciel Zerobin que l'association Framasoft propose sur le site :
https://framabin.org
La personnalisation s'appuie sur la librairie Bootstrap dont les fichiers se trouvent dans la [Framanav](https://git.framasoft.org/framasoft/framanav).

![English:](https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Flag_of_the_United_Kingdom.svg/20px-Flag_of_the_United_Kingdom.svg.png) **Framasoft uses GitLab** for the development of its free softwares. Our Github repositories are only mirrors.
If you want to work with us, **fork us on [git.framasoft.org](https://git.framasoft.org)**. (no registration needed, you can sign in with your Github account)

![Français :](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Flag_of_France.svg/20px-Flag_of_France.svg.png) **Framasoft utilise GitLab** pour le développement de ses logiciels libres. Nos dépôts Github ne sont que des mirroirs.
Si vous souhaitez travailler avec nous, **forkez-nous sur [git.framasoft.org](https://git.framasoft.org)**. (l'inscription n'est pas nécessaire, vous pouvez vous connecter avec votre compte Github)

* * *

ZeroBin 0.19 Alpha

==== THIS IS ALPHA SOFTWARE - USE AT YOUR OWN RISKS ====

ZeroBin is a minimalist, opensource online pastebin where the server
has zero knowledge of pasted data. Data is encrypted/decrypted in the
browser using 256 bits AES.

More information on the project page:
http://sebsauvage.net/wiki/doku.php?id=php:zerobin

------------------------------------------------------------------------------

Copyright (c) 2012 Sébastien SAUVAGE (sebsauvage.net)

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from
the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

    1. The origin of this software must not be misrepresented; you must
       not claim that you wrote the original software. If you use this
       software in a product, an acknowledgment in the product documentation
       would be appreciated but is not required.

    2. Altered source versions must be plainly marked as such, and must
       not be misrepresented as being the original software.

    3. This notice may not be removed or altered from any source distribution.

------------------------------------------------------------------------------